# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy.props
from .basenode import ChordataBaseNode, set_dirty_flag
from ..utils import gui
from ..ops import engine

class DumpSettings(bpy.types.PropertyGroup):
    # === Custom Properties ========================================================
    quaternion_log: bpy.props.BoolProperty(
        name="Quaternion Rotation", default=True, update=set_dirty_flag)
    raw_log: bpy.props.BoolProperty(
        name="Raw Sensor Data", default=False, update=set_dirty_flag)
    real_log: bpy.props.BoolProperty(
        name="Real Sensor Data", default=False, update=set_dirty_flag)
    extra_log: bpy.props.BoolProperty(
        name="Extra Bone Data", default=False, update=set_dirty_flag)
    err_log: bpy.props.BoolProperty(
        name="Errors", default=False, update=set_dirty_flag)
    comm_log: bpy.props.BoolProperty(
        name="Communications", default=False, update=set_dirty_flag)

    rotation_log: bpy.props.BoolProperty(
        name="Rotation", default=True, update=set_dirty_flag)
    position_log: bpy.props.BoolProperty(
        name="Position", default=False, update=set_dirty_flag)
    velocity_log: bpy.props.BoolProperty(
        name="Velocity", default=False, update=set_dirty_flag)
    distance_log: bpy.props.BoolProperty(
        name="Distance", default=False, update=set_dirty_flag)

    extra_log: bpy.props.BoolProperty(
        name="Extras", default=False, update=set_dirty_flag)

    log_destination: bpy.props.EnumProperty(
        name="Log Destination",
        description="Where the Dump Node outputs logs",
        items=[
            ("BLENDER", "Blender Console", "Logs to Blender Console"),
            ("SYSTEM", "System Console", "Logs to System Console"),
            ("TEXT", "Text", "Logs to text datablock")
        ],
        default='BLENDER', update=set_dirty_flag)
    text_file: bpy.props.PointerProperty(
        type=bpy.types.Text, name="Text File", update=set_dirty_flag)
    # ==============================================================================


class DumpNode(ChordataBaseNode):
    '''Forwards motion capture data in the network'''
    bl_idname = 'DumpNodeType'
    bl_label = "Dump Node"

    # === Property Group Pointer ===================================================
    settings: bpy.props.PointerProperty(
        type=DumpSettings)
    # ==============================================================================


    def init(self, context):
        self.width = 350.0
        self.inputs.new('DataStreamSocketType', "debug_in")

    def update(self):
        """Override self.update will specialize the behaviour of this node type"""
        return    

    def draw_buttons(self, context, layout):
        logdest_row = layout.row()
        text_row = layout.row()
        # temp_disabled_row = layout.box()
        # temp_disabled_row.label(text= "These datatargets will be available soon")
        # temp_disabled_row.enabled = False
        logdest_row.prop(self.settings, "log_destination", expand=True)
        if self.settings.log_destination == "TEXT":
            text_row.prop(self.settings, "text_file")
        if self.inputs['debug_in'].is_linked:
            logdest_row.enabled = True
            text_row.enabled = True
            layout.prop(self.settings, "quaternion_log")
            layout.prop(self.settings, "raw_log")
            layout.separator()
            layout.prop(self.settings, "rotation_log")
            layout.prop(self.settings, "position_log")
            layout.separator()
            layout.prop(self.settings, "comm_log")
            layout.prop(self.settings, "err_log")

            layout.separator()
            layout.prop(self.settings, "extra_log")
            # temp_disabled_row.prop(self.settings, "velocity_log")
            # temp_disabled_row.prop(self.settings, "distance_log")
            # temp_disabled_row.prop(self.settings, "real_log")
            # temp_disabled_row.prop(self.settings, "extra_log")
        else:
            logdest_row.enabled = False
            text_row.enabled = False
            layout.label(text="Connect Notochord Node or Chordata Main Node")


    def draw_label(self):
        return "Dump Node"

    @engine.helper_method
    def _output(self, label, msg):
        data_string = "[{}] {}:{} => {}".format(self.name, label, msg.subtarget, msg.payload)
        if self.id_settings.log_destination == 'SYSTEM':
            print(data_string)
        elif self.id_settings.log_destination == 'BLENDER':  # pragma: no branch
            gui.console_write( data_string )


    @engine.datatarget_handler(engine.DataTarget.Q)
    def out_q(self, packet):
        if (self.id_settings.quaternion_log):
            for msg in packet.get():
                self._output("Q", msg)
                
 
    @engine.datatarget_handler(engine.DataTarget.ROT)
    def out_rot(self, packet):
        if (self.id_settings.rotation_log):
            for msg in packet.get():
                self._output("ROT", msg)

    @engine.datatarget_handler(engine.DataTarget.RAW)
    def out_raw(self, packet):
        if (self.id_settings.raw_log):
            for msg in packet.get():
                self._output("RAW", msg)

    @engine.datatarget_handler(engine.DataTarget.POS)
    def out_pos(self, packet):
        print("DUMP POS")
        if (self.id_settings.position_log):
            for msg in packet.get():
                self._output("POS", msg)

    @engine.datatarget_handler(engine.DataTarget.COMM)
    def out_comm(self, packet):
        if (self.id_settings.comm_log):
            for msg in packet.get():
                self._output("COMM", msg)

    @engine.datatarget_handler(engine.DataTarget.ERR)
    def out_err(self, packet):
        if (self.id_settings.err_log):
            for msg in packet.get():
                self._output("ERR", msg)
 
    @engine.datatarget_handler(engine.DataTarget.EXTRA)
    def out_extra(self, packet):
        if (self.id_settings.extra_log):
            for msg in packet.get():
                data_string = "[EXTRA] {}:{} ({}) => {}".format(self.name, msg.subtarget, msg.addrtail, msg.payload)
                if self.id_settings.log_destination == 'SYSTEM':
                    print(data_string)
                elif self.id_settings.log_destination == 'BLENDER':  # pragma: no branch
                    gui.console_write( data_string )
     

to_register = (DumpSettings, DumpNode,)
