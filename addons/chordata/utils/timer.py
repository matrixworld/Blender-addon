# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import time
from collections import deque
from functools import reduce
from .. import defaults

class timekeeper:
	def __init__(self, max_queue_len = defaults.TIMEKEEPER_HIST_LEN):
		self.max_queue_len = max_queue_len
		self.restart()

	def set_checkpoint(self):
		self.checkpoint = time.time()

	def get_checkpoint_delta(self):
		return time.time() - self.checkpoint

	def lap(self):
		current = time.time()
		self.hist.append(current - self.last)
		self.last = current
		self.lap_n +=1

	def avg_lap(self):
		total = reduce(lambda x, y: x+y, self.hist)
		return total / len(self.hist)

	def get_rate_avg(self):
		return 1/self.avg_lap()

	def get_last_lap(self):
		return (self.hist[0] + self.hist[1]) / 2

	def get_rate(self):
		return 1/self.get_last_lap()

	def restart(self):
		self.start = time.time()
		self.checkpoint = self.start 
		self.hist = deque(maxlen = self.max_queue_len)  
		self.hist.append(self.start)
		self.last = self.start
		self.lap_n = 0

	def set_queue_len(self, new_length):
		self.max_queue_len = new_length

	def ellapsed_time(self):
		return time.time() - self.start

	def ellapsed_frames(self):
		pass		

class Stats(timekeeper):
	def __init__(self, max_queue_len = defaults.TIMEKEEPER_HIST_LEN):
		super().__init__(max_queue_len)
		self.msgs_n = 0

	def received_msg(self):
		self.msgs_n += 1
