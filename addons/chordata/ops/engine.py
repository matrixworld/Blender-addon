# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from ..copp_server.defaults import DataTarget
from ..copp_server import COPP_Base_packet
import functools
from copy import deepcopy
from types import SimpleNamespace
from ..nodes.basenode import ChordataBaseNode
import bpy
from ..utils import out, timer
import types



class ID_data_settings_getter():  # pragma: no cover
    """This base class exposes some properties used to get fresh references
    of some blender objects during the execution of time extended operators (like modals).
    This adds some overhead, but avoids crashes due to dangling references

    WARNING! all these getters might raise KeyError if the tree or node is not present
    """
    @property
    def id_settings(self):
        # self._id_settings = bpy.data.node_groups[self.from_tree].nodes[self.from_node].settings
        return bpy.data.node_groups[self.from_tree].nodes[self.from_node].settings

    @property
    def id_node(self):
        return bpy.data.node_groups[self.from_tree].nodes[self.from_node]

    @property
    def id_tree(self):
        return bpy.data.node_groups[self.from_tree]
    


class Engine():
    report_types= {
    'DEBUG': out.debug, 
    'INFO': out.info,
    'WARNING': out.warning, 
    'ERROR': out.error
    }

    def __init__(self, report_fn = None):
        if report_fn:
            self.report = report_fn
        else:
            self.report = self._report

    def update_context(self, context):
        if type(context) != bpy.types.Context:
            raise TypeError("Incorrect context passed to Engine.\nType:{}".format(type(context)))

        self.context = context

    @classmethod
    def _report(cls, report_type, msg):
        try:
            cls.report_types[report_type.pop()](msg)

        except KeyError as e:
            out.error("This type of report is not valid: {}.\n{}".format(report_type, e))

        except Exception as e:
            out.error("Unknown exception while reporting with type {}.\n{}".format(report_type, e))


    def parse_tree(self, node_root, fetch_fns_by_name = False):
        self.engine_root = EngineNode(node_root, self, fetch_fns_by_name)
        to_parse = [{'current_node': node_root, 'current_engine_node': self.engine_root}]
        for branch in to_parse:
            for output in branch['current_node'].outputs:
                if output.is_linked:
                    for link in output.links:
                        try:
                            child = EngineNode(link.to_node, self, fetch_fns_by_name)
                            branch['current_engine_node'].connect(child)
                            to_parse.append(
                                {'current_node': link.to_node, 'current_engine_node': child})
                        except TypeError as e:
                            out.debug("Not valid EngineNode {}. Error: {}".format(link.to_node.name, e))

        node_root.id_data.dirty = False
        
        return self.engine_root

    def cleanup(self):
        self.engine_root.cleanup()


# ==================================
# =           DECORATORS           =
# ==================================

def datatarget_handler(datatarget):
    """Factory for Decorator to mark DataTarget handlers."""
    def wrapper(func):
        if not hasattr(func, "_datatarget_handlers"):
            func._datatarget_handlers = [datatarget]
        else:
            func._datatarget_handlers.append(datatarget)
        return func #returning the unmodified function
    return wrapper

def helper_method(func):
    """Decorator to mark helper functions."""
    func._is_helper_method = True
    return func 

# ======  End of DECORATORS  =======

class EngineNode(ID_data_settings_getter):
    ''' Base class for linked nodes logic '''

    @property
    def settings(self):
        raise AttributeError("""The `settings` attribute is not available whitin an EngineNode. 
Use `id_settings` to access the node's settings""")
    
    
    def __init__(self, node, engine = None, fetch_by_name = False):
        if not isinstance(node, ChordataBaseNode):
            raise TypeError("The node {} is not an instance of ChordataBaseNode class".format(node.name))

        self.from_tree = node.id_data.name
        self.from_node = node.name
        self.node_class = type(node)
        self.name = node.name[:]
        self.children = []
        self.parent = None
        self.engine = engine
        
        if "stats" in node.outputs and node.outputs["stats"].bl_idname == 'StatsSocketType':
            self.stats = timer.Stats()
        else:
            self.stats = None

        # self.settings = {}
        
        # Dictionary with all DataTargets associated with the default_handler 
        self.exposed_handlers = { t : self.default_handler for t in DataTarget }
        self.exposed_handlers[None] = self.no_target_handler

        helpers_info = []
        handlers_info = {}
        #Fetch functions based on marker decorators
        for fn in self.node_class.__dict__.values():

            # Fetch helper functions and bind them to the EngineNode
            if hasattr(fn, "_is_helper_method"):
                setattr(self, fn.__name__,  types.MethodType(fn, self))
                helpers_info.append(fn.__name__)

            # Fetch datatarget handlers and put them in the 'exposed_handlers' dict
            elif hasattr(fn, "_datatarget_handlers"):
                for target in fn._datatarget_handlers:
                    self.exposed_handlers[target] = functools.partial(fn, self=self)
                    handlers_info[target] = fn.__name__

        # Fetch functions based on name
        if fetch_by_name:
            for target in DataTarget:
                if hasattr(self.node_class, target.value + '_handler'):
                    self.exposed_handlers[target] = functools.partial(getattr(
                        self.node_class, target.value + '_handler'), self=self)
                    handlers_info[target] = target.value + '_handler'

        handlers_info = ["- {}: {}".format(k,v) for k,v in handlers_info.items()]

        out.debug("EngineNode [{:>20}]\n## Handlers:\n{}\n## Helpers:\n- {}".format(self.name,
        "\n".join(handlers_info),
        "\n- ".join(helpers_info)))

         #run the init function of the engine nodes
        functools.partial(
            getattr(self.node_class, 'on_engine_node_init'), self=self)()

         # End of __init__
    
    def report(self, report_type, msg):
        if self.engine:
            self.engine.report(report_type, msg)
            return

        Engine._report(report_type, msg)

    def cleanup(self):
        #run the stop function of the engine nodes
        functools.partial(
            getattr(self.node_class, 'on_engine_node_stop'), self=self)()

        for child in self.children:
            child.cleanup()
        

    # ==============================================
    # =           PACKET PROCESS METHODS           =
    # ==============================================
    
    def __call__(self, packet):
        if self.stats and isinstance(packet, COPP_Base_packet):
            self.stats.lap()
            # TODO, calculate this just once in a while
            self.id_node.outputs["stats"].config.packet_rate = self.stats.get_rate()
            self.id_node.outputs["stats"].config.packets_n = self.stats.lap_n
            self.id_node.outputs["stats"].config.msgs_n = self.stats.msgs_n
            
            
        return self.process_packet(packet)

    def process_packet(self, packet):
        ret = self.exposed_handlers[packet.target](packet=packet)
        if ret != None:
            for child in self.children:
                child(ret)
        # this return is for testing purposes only
        return ret


    def no_target_handler(self, packet):
        for msg in packet.get():
            self.process_packet(msg)
            if self.stats:
                self.stats.received_msg()

    def default_handler(self, packet=None):
        out.debug( "DEF HANDLER @ %s %s", self.name, str(packet) )
        return packet

    # ======  End of PACKET PROCESS METHODS  =======

    def connect(self, node):
        reached_root = False
        current_node = self
        allowed = False
        # iterate in parents to check for loops
        while not reached_root:
            if current_node.parent != None:
                if current_node.parent == node:
                    allowed = False
                    break
                else:
                    current_node = current_node.parent
            else:
                reached_root = True
                allowed = True
        if allowed:
            self.children.append(node)
            node.parent = self