#!/bin/bash


if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
	echo usage: $0 VERSION_MAYOR VERSION_MINOR VERSION_PATCH [$HASH]
	exit 1
fi 

VERSION_MAYOR="$1"
VERSION_MINOR="$2"
VERSION_PATCH="$3"

CURRENT_VERSION="($VERSION_MAYOR, $VERSION_MINOR, $VERSION_PATCH)"
WARN_COMMENT='#!! Warning is overwritten by the script `set_version.sh` which runs in CI'

if [ -n "$4" ]; then
	CURRENT_HASH="$4"
else
	CURRENT_HASH=$CI_COMMIT_SHORT_SHA
fi


if [ -n "$CURRENT_HASH" ]; then
	CURRENT_HASH="'#$CURRENT_HASH'"
fi

if [ -z "$CURRENT_HASH" ]; then
	CURRENT_HASH="'dev#$(git rev-parse --short HEAD)'"
fi

if [ -z "$CURRENT_HASH" ]; then
	CURRENT_HASH="'dev'"
fi


sed -i -e "s/GIT_HASH *=.*/GIT_HASH = $CURRENT_HASH $WARN_COMMENT/g" addons/chordata/defaults.py

sed -i -e "s/ADDON_VERSION *=.*/ADDON_VERSION = $CURRENT_VERSION $WARN_COMMENT/g" addons/chordata/defaults.py

sed -i -e "s/GIT_HASH *=.*/GIT_HASH = $CURRENT_HASH $WARN_COMMENT/g" addons/create_zip_file.py

sed -i -e "s/ADDON_VERSION *=.*/ADDON_VERSION = $CURRENT_VERSION $WARN_COMMENT/g" addons/create_zip_file.py

sed -i -e "s/\"version\" *:.*/\"version\": $CURRENT_VERSION, $WARN_COMMENT/g" addons/chordata/__init__.py