# Chordata client [Blender Addon]
_Motion capture manager for the Chordata Open Source motion capture system_

This software allows you to receive, record, and retransmit physical motion capture data coming from a [Chordata open source motion capture system](http://chordata.cc)

[![pipeline status](https://gitlab.com/chordata/Blender-addon/badges/master/pipeline.svg)](https://gitlab.com/chordata/Blender-addon/pipelines) [![coverage report](https://gitlab.com/chordata/Blender-addon/badges/master/coverage.svg)](https://gitlab.com/chordata/Blender-addon/-/jobs/artifacts/master/file/tests/htmlcov/index.html?job=coverage)


![chordata_blender_addon_screenshot](https://chordata.cc/wp-content/uploads/2020/03/new-blender-addon-beta-version-released-chordata-motion.jpeg)

## Installation

Download the last stable release from our [Downloads page](https://chordata.cc/downloads),
or get the automatically generated .zip here:

[![chordata_addon_download_badge](https://img.shields.io/badge/Download%20zip%20-Chordata%20addon-orange)](https://gitlab.com/chordata/Blender-addon/-/jobs/artifacts/master/browse?job=zip)

Install the .zip on the Blender preferences dialog.

## Usage

You will need the Chordata Motion hardware to use this addon. It can be build by you or you can get one on our upcoming crowdfounding campaign. More info [here](https://chordata.cc/pre-order)

Follow [this thread on our forum to know more](https://forum.chordata.cc/d/62-new-blender-2-8-chordata-node-system-addon)


### Cloning from git
If you want to clone this repository don't forget to pull the content from the dependencies included as git submodules

```bash
git submodule init
git submodule update --recursive --remote
```

### LICENSE

This program is free software and is distributed under the GNU General Public License, version 3.
You are free to run, study, share and modify this software.
Derivative work can only be distributed under the same license terms, and replicating this program copyright notice.

More info about the terms of the license [here](https://www.gnu.org/licenses/gpl-faq.en.html). 