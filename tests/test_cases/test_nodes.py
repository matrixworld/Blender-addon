def test_nodes_links(chordata_nodetree, engine, chordata_module, mocker):
    t_node = chordata_nodetree.nodes.new('TestCubeNodeType')
    n_node = chordata_nodetree.nodes.new('NotochordNodeType')
    socket_out = n_node.outputs['stats']
    socket_in = t_node.inputs['capture_in']
    
    #nodes should avoid creating links from different socket types
    chordata_nodetree.links.new(socket_in, socket_out)
    assert not t_node.inputs[0].links

    #the verify_links_valid checks and deletes invalid links
    chordata_module.nodes.basenode.verify_links_valid = mocker.Mock()
    chordata_nodetree.links.new(socket_in, socket_out)
    assert t_node.inputs[0].links[0].from_node == n_node
    chordata_module.nodes.basenode.verify_links_valid.assert_called()


def test_custom_node(bpy_test, chordata_nodetree, engine, copp_Q_packet, mocker):
    c_node = chordata_nodetree.nodes.new('CustomNodeType')
    c_eng_node = engine.EngineNode(c_node)

    c_eng_node.id_settings.dump_to_console = True
    c_eng_node._output = mocker.Mock()
    n_elemns = len(copp_Q_packet._elements)
    c_eng_node(copp_Q_packet)
    c_eng_node._output.assert_called()
    assert c_eng_node._output.call_count == n_elemns
    
    c_eng_node._output.reset_mock()
    copp_Q_packet.restore()
    c_eng_node.id_settings.dump_to_console = False
    c_eng_node(copp_Q_packet)
    c_eng_node._output.assert_not_called()


    copp_Q_packet.restore()
    c_eng_node.id_settings.rotate_current_ob = True
    bpy_test.ops.mesh.primitive_monkey_add()
    prev_rotation = bpy_test.context.object.rotation_euler.copy() 
    c_eng_node(copp_Q_packet)
    assert bpy_test.context.object.rotation_euler != prev_rotation