import pytest
import time

@pytest.fixture
def timekeeper(chordata_module):
	return chordata_module.utils.timer.timekeeper()

def test_timer_basic(timekeeper, chordata_defaults):
	assert abs(timekeeper.start - time.time()) < 0.2

	rate = 100 #in Hz
	for i in range(chordata_defaults.TIMEKEEPER_HIST_LEN + 2):
		timekeeper.lap()
		time.sleep(1/rate)

	assert len(timekeeper.hist) == chordata_defaults.TIMEKEEPER_HIST_LEN
	assert timekeeper.avg_lap() == pytest.approx(1/rate, abs=0.001)
	assert timekeeper.get_last_lap() == pytest.approx(1/rate, abs=0.001)
	assert timekeeper.get_rate() == pytest.approx(rate, abs=4)

def test_node_stats(base_fake_nodetree, engine, copp_Q_packet, chordata_defaults):
	t_node, n_node, f_node = base_fake_nodetree
	
	assert 'stats' in n_node.outputs
	assert n_node.outputs['stats'].config.track_stats == True
	assert 'stats' not in f_node.outputs
	assert engine.EngineNode(f_node).stats is None

	engine_instance = engine.Engine()
	engine_root = engine_instance.parse_tree(n_node)

	rate = 60 #in Hz
	for i in range(chordata_defaults.TIMEKEEPER_HIST_LEN + 2):
		engine_root(copp_Q_packet)
		time.sleep(1/rate)

	assert engine_root.stats.get_rate() == pytest.approx(rate, rel=6)
