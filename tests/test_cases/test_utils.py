import pytest

def test_Mocap_workspace_nodeeditors(bpy_test, chordata_module):
	assert type(chordata_module.utils.gui.nodeeditor_spaces_get("current")) == list
	
	with pytest.raises(KeyError, match="No workspace named.*"):
		chordata_module.utils.gui.nodeeditor_spaces_get("Pippo")

	bpy_test.ops.chordata.workspace_add()
	assert len(chordata_module.utils.gui.nodeeditor_spaces_get()) == 1
	#WARNING! this cleanup function is not working! we might be adding several 
	#`Mocap` workspaces during the testsuite!
	#see RemoveChordataWorkspace(bpy.types.Operator) in templates/workspace.py
	bpy_test.ops.chordata.workspace_rm()


def test_Console_write(bpy_test, chordata_module):
	bpy_test.ops.chordata.workspace_add()

	res = chordata_module.utils.gui.console_write("test", workspace="Layout")
	assert res == False

	res = chordata_module.utils.gui.console_write("test", workspace="Mocap")
	assert res == True

	#WARNING! this cleanup function is not working! we might be adding several 
	#`Mocap` workspaces during the testsuite!
	#see RemoveChordataWorkspace(bpy.types.Operator) in templates/workspace.py
	bpy_test.ops.chordata.workspace_rm()


def test_get_OP_blender_type(bpy_test, chordata_module):
	fn = chordata_module.utils.get_OP_blender_type

	ret1 = fn("chordata.avatar_add")
	ret2 = fn("avatar_add")

	assert ret2 == ret1
