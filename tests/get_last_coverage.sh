#!/bin/bash

set -eu

COVERAGE_URL="https://gitlab.com/chordata/Blender-addon/-/jobs/artifacts/${CI_COMMIT_BRANCH}/raw/tests/coverage.xml?job=coverage"
echo "Getting last coverage report from branch $CI_COMMIT_BRANCH"
echo "Url: $COVERAGE_URL"

curl -L $COVERAGE_URL > last_coverage.xml